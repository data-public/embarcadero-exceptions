//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "borport.h"
#if !defined WITHOUT_PERL
#include "totperl.h"
#endif
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm2::Prepare()
{
#if !defined WITHOUT_PERL
	if (!Form2)
		throw BorPort::Exception("!Form2");
  try
	{
		DataKernel::PerlAPI* lpPerl=DataKernel::PerlAPI::GetDBIPerl();
		// BD: pl_odbc
		Form2->Memo1->Lines->Add("pl_odbc");
		TStringList* lDBI = new TStringList;
		lpPerl->GetDrivers(lDBI);
		delete lpPerl;

		for (int li=0; li < lDBI->Count; ++li)
		{
			AnsiString lDriver=lDBI->Strings[li];
			Form2->Memo1->Lines->Add("DBI:" + lDriver);
		}
	}
	catch (BorPort::Exception& Ex)
	{
		TOTDebug(("Cannot add perl drivers: %s", Ex.Message.c_str()));
		Application->MessageBox(L"get_message()", L"catch!");
		pt::string m=Ex.get_message();
		AnsiString s((const char*)m);
		UnicodeString w(s);
		Application->MessageBox(w.c_str(), L"catch!");
	}
#else
	if (Form2)
		throw BorPort::Exception("Test");
		//throw new pt::exception("Test");
		//throw "Test";
#endif
}
//---------------------------------------------------------------------------

