#if !defined __BORPORT_H_
#define      __BORPORT_H_

#if defined USE_PTYPES
#include <ptypes.h>
#else
#include <string>
#include <stdexcept>
#endif

namespace BorPort {
	#if defined USE_PTYPES
	class Exception : public pt::exception {
		public:
			Exception(const char* str) : pt::exception(str) {}
			Exception(const Exception& ex) : pt::exception(const_cast<Exception&>(ex).get_message()) {}
	};

	class AnsiString : public pt::string
	{
		public:
			AnsiString() {}
			AnsiString(const char* s) : pt::string(s) {}
			AnsiString(const pt::string& a) : pt::string(a) {}
			AnsiString(const AnsiString& a) : pt::string(a) {}
      AnsiString(int a) : pt::string(pt::itostring(a)) {}
			const char* c_str() const { return (const char*)(*this); }
      int Length() const { return pt::length(*this); }
			AnsiString(const System::UnicodeString& s) : pt::string(System::AnsiString(s).c_str()) {}

			operator System::UnicodeString() { return System::AnsiString((const char*)(*this)); }
	};
	#else
	class Exception : public std::exception {
    std::string msg;
		public:
			Exception(const char* str) : msg(str) {}
			Exception(const Exception& ex) : msg(ex.what()) {}
			virtual const char* what() const noexcept { return msg.c_str(); }
      std::string get_message() const { return msg; }
	};
	#endif

	class Variant : public System::Variant {};
};

#define NAMESPACE_DECL_BORPORT_BEGIN(x) namespace x {
#define NAMESPACE_DECL_BORPORT_END(x) }

#define NAMESPACE_IMPL_BORPORT_BEGIN(x) namespace x {
#define NAMESPACE_IMPL_BORPORT_END(x) }

#define TOTDebugInitFunc()
#define TOTDebug(x)
#define TOTDebugError(x)
#define TOTDebugInfo(x)

#if defined USE_PTYPES
static pt::string gDirectorySeparator("\\");
#endif

#endif
