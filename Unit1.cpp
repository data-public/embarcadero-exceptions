//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "borport.h"
#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
static HANDLE outfile = INVALID_HANDLE_VALUE;
static void _do_write(const char* s, int l)
{
	if (outfile != INVALID_HANDLE_VALUE)
	{
		DWORD out;
    TCHAR date[64];
		int len = GetTimeFormat(LOCALE_INVARIANT, 0, 0, TEXT("HH':'mm':'ss "), date, 63);
		::WriteFile(outfile, date, (len + 1) * sizeof(TCHAR), &out, 0);
		::WriteFile(outfile, s, l, &out, 0);
		if (s[l - 1] != '\n')
			::WriteFile(outfile, "\r\n", 2, &out, 0);
  }
}
static void _do_write(const char* s) { _do_write(s, strlen(s)); }
//---------------------------------------------------------------------------
#if defined WITH_ONEX
void __fastcall TForm1::OnEx(TObject* o, Sysutils::Exception* ex)
{
  _do_write("in OnEx");
	Application->MessageBox(L"??", L"OnEx!");
	UnicodeString w(ex->Message);
	Application->MessageBox(w.c_str(), L"OnEx!");
}
#endif
//---------------------------------------------------------------------------
static void test_unexp()
{
	_do_write("in std::unexpected");
	Application->MessageBox(L"test_unexp()", L"catch!");
}
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	outfile = ::CreateFile(L"out.log",           // name of the write
											 GENERIC_WRITE,          // open for writing
											 FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,
											 NULL,                   // default security
											 CREATE_NEW,             // create new file only
											 FILE_ATTRIBUTE_NORMAL,  // normal file
											 NULL);                  // no attr. template
	if (outfile == INVALID_HANDLE_VALUE)
		outfile = ::CreateFile(L"out.log",         // name of the write
											 GENERIC_WRITE,          // open for writing
											 FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE,
											 NULL,                   // default security
											 TRUNCATE_EXISTING,      // truncate existing file
											 FILE_ATTRIBUTE_NORMAL,  // normal file
											 NULL);                  // no attr. template
	if (outfile == INVALID_HANDLE_VALUE)
		Application->MessageBox(L"Cannot create nor truncate file", L"error");
	else
		_do_write("File opened");
	std::set_unexpected(test_unexp);
#if defined WITH_ONEX
	Application->OnException = &OnEx;
#endif
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	try
	{
		_do_write("before Prepare()");
		TForm2::Prepare();
    _do_write("after Prepare()");
	}
	catch (const BorPort::Exception& ex)
	{
		_do_write("catch BorPort::Exception");
		Application->MessageBox(L"get_message()", L"catch!");
		#if defined USE_PTYPES
		pt::string m=const_cast<BorPort::Exception&>(ex).get_message();
		AnsiString s((const char*)m);
		#else
		AnsiString s(ex.what());
		#endif
		UnicodeString w(s);
		Application->MessageBox(w.c_str(), L"catch!");
	}
	catch (const char* ex)
	{
		_do_write("catch char*");
		AnsiString s(ex);
		UnicodeString w(s);
		Application->MessageBox(w.c_str(), L"catch!");
	}
	catch (...)
	{
		_do_write("catch ...");
		Application->MessageBox(L"...", L"catch!");
	}
	_do_write("before Show()");
	Form2->Show();
	_do_write("after Show()");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
	if (outfile != INVALID_HANDLE_VALUE)
	{
		HANDLE _out = outfile;
    _do_write("Closing file");
		outfile = INVALID_HANDLE_VALUE;
		CloseHandle(_out);
	}
}
//---------------------------------------------------------------------------

